from tkinter import *
import Player
character = Player.Player('butt', 5, 20, 0, 1, 5, 0)
import sys
sys.path.append('../..')
from monsters import zombie
from monsters import skeleton
from monsters import rat
from monsters import landoctopus
from monsters import shekelton
from monsters import ceilingjiggly
from monsters import jackpizza
monster = skeleton.Skeleton('Skeleton', 3, 7, 50, 2)

'''
monster = zombie.Zombie('Zombie', 2, 5, 30, 1)
monster = rat.Rat('Rat', 1, 2, 10, 3)
monster = landoctopus.Landoctopus('Mutant Octopus', 5, 11, 80, 2)
monster = shekelton.Shekelton('Lord Shekelton', 6, 15, 400, 3)
monster = ceilingjiggly.Ceilingjiggly('Ceiling Jiggly', 3, 3, 10, 1)
monster = jackpizza.Jackpizza('Jack Pizza', 7, 30, 0, 5)


class Combat:

    def __init__(self, player, monster, money):

        self.main_window = Tk()
        frame = Frame(self.main_window, width=200, height=200)
        frame.pack()

        self.__monster = skeleton
        #self.__character = player
        self.__money = money
        self.turncount = 1
        self.defendcount = 0

        self.top_frame = Frame(self.main_window)
        self.bottom_frame = Frame(self.main_window)
        self.health_label = Label(self.main_window)

        #self.bottom_frame.configure(background='black')

        self.value = StringVar()
        self.text = Label(self.bottom_frame, textvariable = self.value)
        self.value.set("Oh no, a " + monster.get_name() + " is attacking you!  What do you do?")
        self.quit = Button(self.bottom_frame, text = 'Exit battle', command = self.endbattle)

        #define buttons for combat
        self.attackbutton = Button(self.bottom_frame, text = 'Attack', command = self.attack)
        self.potionbutton = Button(self.bottom_frame, text = 'Drink a health potion', command = self.potion)
        self.defendbutton = Button(self.bottom_frame, text = 'Take a defensive stance', command = self.defend)
        self.main_window.attributes("-topmost", True)
        #pack everything
        self.bottom_frame.pack()
        self.top_frame.pack()
        self.text.pack()
        self.attackbutton.pack()
        self.potionbutton.pack()
        self.defendbutton.pack()
        mainloop()

    def endbattle(self):
        self.main_window.destroy()

    def attack(self):
        if monster.get_speed() > character.get_speed():
            if self.defendcount >= 0:
                characternewhealth = character.get_health() - (monster.get_attack() * 0.7)
                characternewhealth = round(characternewhealth, 1)
                character.set_health(characternewhealth)
                skeletonnewhealth = monster.get_health() - character.get_attack()
                monster.set_health(skeletonnewhealth)
                self.value.set("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                self.defendcount -= 1
                self.turncount += 1
            else:
                characternewhealth = character.get_health() - monster.get_attack()
                characternewhealth = round(characternewhealth, 1)
                character.set_health(characternewhealth)
                skeletonnewhealth = monster.get_health() - character.get_attack()
                monster.set_health(skeletonnewhealth)
                self.value.set("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                self.turncount += 1

        if monster.get_speed() <= character.get_speed():
            if self.defendcount >= 0:
                skeletonnewhealth = monster.get_health() - character.get_attack()
                monster.set_health(skeletonnewhealth)
                characternewhealth = character.get_health() - (monster.get_attack() / 3)
                characternewhealth = round(characternewhealth, 1)
                character.set_health(characternewhealth)
                monster.set_health(skeletonnewhealth)
                self.value.set("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                self.defendcount -= 1
                self.turncount += 1
            else:
                characternewhealth = character.get_health() - monster.get_attack()
                characternewhealth = round(characternewhealth, 1)
                character.set_health(characternewhealth)
                skeletonnewhealth = monster.get_health() - character.get_attack()
                monster.set_health(skeletonnewhealth)
                self.value.set("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))

        if character.get_health() <= 0:
            self.value.set('You deed')
            self.quitbutton = Button(self.bottom_frame, text = 'Exit battle', command = self.endbattle)
            self.attackbutton.destroy()
            self.potionbutton.destroy()
            self.defendbutton.destroy()
            self.quitbutton.pack()

        if monster.get_health() <= 0:
            tempgold = character.get_money() + monster.get_money()
            character.set_money(tempgold)
            self.value.set("You defeated the monster! The monster dropped " + str(monster.get_money()) + " gold" + '\n' + "Your health is " + str(character.get_health()) + "\n" + "You now have " + str(character.get_money()) + " gold")
            self.quitbutton = Button(self.bottom_frame, text = 'Exit battle', command = self.endbattle)
            self.attackbutton.destroy()
            self.potionbutton.destroy()
            self.defendbutton.destroy()
            self.quitbutton.pack()

    def potion(self):
        if character.get_potions() <= 0:
            self.value.set("You have no more health potions!")
            self.turncount += 1

        else:
            characterhealth = character.get_health() + 10
            characterhealth = round(characterhealth, 1)
            character.set_health(characterhealth)
            newpotions = character.get_potions() - 1
            character.set_potions(newpotions)
            if self.defendcount >= 0:
                characternewhealth = character.get_health() - (monster.get_attack() / 3)
                character.set_health(characternewhealth)
                self.value.set("Your health is " + str(character.get_health()) + '\n' + "Number of potions you still have " + str(character.get_potions()) + '\n' + "The monster's health is " + str(monster.get_health()))
                self.defendcount -= 1
            else:
                characternewhealth = characterhealth - monster.get_attack()
                characternewhealth = round(characternewhealth, 1)
                character.set_health(characternewhealth)
                self.value.set("Your health is " + str(character.get_health()) + '\n' + "Number of potions you still have " + str(character.get_potions()) + '\n' + "The monster's health is " + str(monster.get_health()))
                self.turncount += 1

    def defend(self):
        characternewhealth = character.get_health() - (monster.get_attack() / 3)
        characternewhealth = round(characternewhealth, 1)
        character.set_health(characternewhealth)
        self.value.set("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
        self.defendcount += 3
        self.turncount += 1

        #combat = Combat(character, monster, character.get_money())
'''