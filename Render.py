# Griffin Moran
x = 0
y = 0
from tkinter import *
from combat import *
#from store import *
import start


class Ball:
    def __init__(self, canvas, img, x1, y1) -> object:
        self.x1 = x1
        self.y1 = y1

        self.canvas = canvas
        self.ball = self.canvas.create_image(x1, y1, image=img)

        self.canvas.bind("<w>", self.up)
        self.canvas.bind("<s>", self.down)
        self.canvas.bind("<a>", self.left)
        self.canvas.bind("<d>", self.right)
        self.canvas.focus_set()
        self.currentx = 100
        self.currenty = 100

    def store(self):
        newarmor = character.get_armor() + 1
        character.set_armor(newarmor)
        newattack = character.get_attack() + 3
        character.set_attack(newattack)
        newpotions = character.get_potions() + 1
        character.set_potions(newpotions)
        print("Your attack is ", character.get_attack())
        print("Your armor is ", character.get_armor())

    def combat(self):
        defendcount = 0
        turncount = 0
        while character.get_health() > 0 and monster.get_health() > 0:
            decision = input('What do you want to do? a. Attack with your sword b. Consume a health potion c. Take a defensive stance ')
            if decision == 'a' or 'A':
                if monster.get_speed() > character.get_speed():
                    if defendcount >= 0:
                        characternewhealth = character.get_health() - (monster.get_attack() * 0.7)
                        characternewhealth = round(characternewhealth, 1)
                        character.set_health(characternewhealth)
                        skeletonnewhealth = monster.get_health() - character.get_attack()
                        monster.set_health(skeletonnewhealth)
                        print("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                        defendcount -= 1
                        turncount += 1
                    else:
                        characternewhealth = character.get_health() - monster.get_attack()
                        characternewhealth = round(characternewhealth, 1)
                        character.set_health(characternewhealth)
                        skeletonnewhealth = monster.get_health() - character.get_attack()
                        monster.set_health(skeletonnewhealth)
                        print("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                        turncount += 1

                elif monster.get_speed() <= character.get_speed():
                    if defendcount >= 0:
                        skeletonnewhealth = monster.get_health() - character.get_attack()
                        monster.set_health(skeletonnewhealth)
                        characternewhealth = character.get_health() - (monster.get_attack() / 3)
                        characternewhealth = round(characternewhealth, 1)
                        character.set_health(characternewhealth)
                        monster.set_health(skeletonnewhealth)
                        print("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                        defendcount -= 1
                        turncount += 1
                    else:
                        characternewhealth = character.get_health() - monster.get_attack()
                        characternewhealth = round(characternewhealth, 1)
                        character.set_health(characternewhealth)
                        skeletonnewhealth = monster.get_health() - character.get_attack()
                        monster.set_health(skeletonnewhealth)
                        print("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))

                elif character.get_health() <= 0:
                    print('You deed')
                    break

                elif monster.get_health() <= 0:
                    tempgold = character.get_money() + monster.get_money()
                    character.set_money(tempgold)
                    print("You defeated the monster! The monster dropped " + str(monster.get_money()) + " gold" + '\n' + "Your health is " + str(character.get_health()) + "\n" + "You now have " + str(character.get_money()) + " gold")
                    break

            elif decision == 'b' or 'B':
                if character.get_potions() <= 0:
                    print("You have no more health potions!")
                    turncount += 1

                else:
                    characterhealth = character.get_health() + 10
                    characterhealth = round(characterhealth, 1)
                    character.set_health(characterhealth)
                    newpotions = character.get_potions() - 1
                    character.set_potions(newpotions)
                    if defendcount >= 0:
                        characternewhealth = character.get_health() - (monster.get_attack() / 3)
                        character.set_health(characternewhealth)
                        print("Your health is " + str(character.get_health()) + '\n' + "Number of potions you still have " + str(character.get_potions()) + '\n' + "The monster's health is " + str(monster.get_health()))
                        defendcount -= 1
                    else:
                        characternewhealth = characterhealth - monster.get_attack()
                        characternewhealth = round(characternewhealth, 1)
                        character.set_health(characternewhealth)
                        print("Your health is " + str(character.get_health()) + '\n' + "Number of potions you still have " + str(character.get_potions()) + '\n' + "The monster's health is " + str(monster.get_health()))
                        turncount += 1
            elif decision == 'c' or 'C':
                characternewhealth = character.get_health() - (monster.get_attack() / 3)
                characternewhealth = round(characternewhealth, 1)
                character.set_health(characternewhealth)
                print("Your health is " + str(character.get_health()) + '\n' + "The monster's health is " + str(monster.get_health()))
                defendcount += 3
                turncount += 1

    def up(self, event):
        y = 0
        y -= 10
        self.currenty -= 10
        self.canvas.move(self.ball, 0, y)
        if 393 < self.currentx < 966 and 519 < self.currenty < 863:
            self.store()
        if 1273 < self.currentx < 1406 and 109 < self.currenty < 196:
            self.combat()

    def down(self, event):
        y = 0
        y += 10
        self.currenty += 10
        self.canvas.move(self.ball, 0, y)
        if 393 < self.currentx < 966 and 519 < self.currenty < 863:
            self.store()
        if 1273 < self.currentx < 1406 and 109 < self.currenty < 196:
            self.combat()

    def left(self, event):
        x = 0
        x -= 10
        self.currentx -= 10
        self.canvas.move(self.ball, x, 0)
        if 393 < self.currentx < 966 and 519 < self.currenty < 863:
            self.store()
        if 1273 < self.currentx < 1406 and 109 < self.currenty < 196:
            self.combat()

    def right(self, event):
        x = 0
        x += 10
        self.currentx += 10
        self.canvas.move(self.ball, x, 0)
        if 393 < self.currentx < 966 and 519 < self.currenty < 863:
            self.store()
        if 1273 < self.currentx < 1406 and 109 < self.currenty < 196:
            self.combat()

# Create Window
Rwindow = Tk()
Rwindow.title("Pizza Wars: Attack of Jack")

# Setup the canvas with a background images
mapWidth = 1600
mapHeight = 500
map = PhotoImage(file="city.gif")
Rcanvas = Canvas(Rwindow, width=mapWidth, height=mapHeight, bg="white")
Rcanvas.create_image(mapWidth / 2, mapHeight / 2, image=map)

# Create the movable object
ballImg = PhotoImage(file=start.start.get_sprite())
ball = Ball(Rcanvas, ballImg, 100, 100)
#
# Pack the canvas
Rcanvas.pack()
Rwindow.attributes("-topmost", True)
Rwindow.mainloop()
